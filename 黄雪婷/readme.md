### 一、类型的访问修饰符和修饰符

1. 访问修饰符
    + public         公共的，可以被任何代码访问
    + internal       只能在当前项目（它所在的）项目所访问
2. 修饰符
    + static         静态的，其成员必然为静态成员
    + abstract       抽象的，无法被实例化，只能被继承
    + sealed         封闭的，无法被继承

### 二、类型成员的访问修饰符和修饰符

1. 访问修饰符
    + public         公共的，可以被类型内部和外部的代码访问
    + private        私有的，只能被类型内部代码访问
    + internal       内部的，只能被当前项目代码访问
    + protected      保护的，只能被当前类型和派生类型代码访问

2. 修饰符

    + 字段
        + readonly 	被 readonly 修饰的字段可以在定义的时候和构造函数中赋值赋值，但是不能在其他地方赋值了，
		如果构造函数中想要赋值需要打this。
        + static	静态字段，不用实例化就行应用。切不能被其他类改变

    + 方法
        + virtual	可被任何继承它的类替代，切不能与 static、abstract, private 或 override 修饰符一起使用
        + abstract	抽象方法只能在抽象类中使用，切不能使用virtual和static修饰，也不能在静态里面修饰abstract
        + static	静态方法只能访问静态成员，不能访问实例变量，也不能this。
        + override	用与重新写virtual，abstract，还有自己的修饰符
        + sealed	密封方法不能被重新写
