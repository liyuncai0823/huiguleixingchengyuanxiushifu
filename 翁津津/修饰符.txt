一、类型的访问修饰符和修饰符

1. 访问修饰符
    + public          公共的，可以在每个项目被访问
    + internal       内部的，只能在当前项目访问 (可省略不写)
2. 修饰符
    + static           静态类，成员都是静态成员
    + abstract       抽象类，不能被实例化，只能被继承
    + sealed          封闭类，不能被继承

二、类型成员的访问修饰符和修饰符

1. 访问修饰符
    + public           公共的，可以在每个类的内外部被访问 
    + private          私有的，只能在每个类的内部被访问，一般被默认  (起到保护，限制作用)
    + internal         内部的，只能在同一个项目被访问，跟类型的internal相似
    + protected      保护的，只能在自身和继承类中被访问

2. 修饰符

    + 字段
        + readonly       只读的，只能读值，不能赋值
        + static            静态的，可以用类名访问，常量不能使用

    + 方法
        + virtual          虚拟的，用于修改方法或属性的声明。
                               不能和static\abstract\override一起使用，静态属性使用是错误
        + abstract       抽象的，不能被实例化，方法由子类实现，静态属性使用是错误
        + static           静态的，成员都是静态成员
        + override      重写的，把父类(虚拟类型)覆盖，让父类内容用子类呈现
        + sealed         封闭的，没有子类，不能被重写方法