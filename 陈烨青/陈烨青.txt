一.类型的访问修饰符和修饰符

1.访问修饰符
     +pubilc  公有的，其他类型和类型成员的访问修饰符，没有访问限制；

     +internal  只能在同一个项目中被访问（其他项目不可使用）

2.修饰符
      +partial   可以将类或者结构，接口（interface）的定义拆分到两个或者更多的源文件。每个文件都包含一部分类型和方法定义；

      +abstract  抽象类的，只能被继承，而子代中需要修饰符（override），也无法被实例化；

      +sealed  封闭的  任何类不得从该类继承；
 
      +static   静态的，他的成员也得事静态，不用实例化对象，可直接使用
二.类型成员访问修饰符和修饰符

1.访问修饰符

      +public  公有的，其他类型和类型成员得访问修饰符，没有访问权限；

      +internal   只能在同一个项目中被访问（其他项目不可使用）；

      +protected  保护的，只能被自己和自己的子类访问，类的实例化都不可以访问；

      +private  私有的，只能是自己的类访问

 2.修饰符
      
   +字段
      +readonly   是在属性修饰符可使用，或者构造函数中初始化，在其他地方赋值会出现错误；

      +static   静态的，他的成员也得事静态，不用实例化对象，可直接使用


   +方法
      +virtual  虚拟的，虚拟继承

      +abstract  抽象类的，只能被继承，而子代中需要修饰符（override），也无法被实例化；

      +static   静态的，他的成员也得事静态，不用实例化对象，可直接使用；

      +override  与abstract是相互使用的，在实例化对象时，只能实例化override的类

      ++sealed  封闭的  任何类不得从该类继承